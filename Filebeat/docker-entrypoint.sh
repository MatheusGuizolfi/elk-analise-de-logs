#!/bin/bash

#Inicia o filebeat
/etc/init.d/filebeat start

set -e

# Add filebeat como comando se necessário
if [ "${1:0:1}" = '-' ]; then
	set -- filebeat "$@"
fi

exec "$@"
