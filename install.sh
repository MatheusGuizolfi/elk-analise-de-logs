#!/bin/bash

#DOCKER NETWORK:

docker network create -d bridge elk-connect

#ELK:

#INSTALAÇÃO DO KIBANA
docker run -ti --name kibana -d --network=elk-connect  matheusguizolfi/kibana /bin/bash

#INSTALAÇÃO DO ELASTICSEARCH
docker run -ti --name elasticsearch -d --network=elk-connect -p 9200:9200 -p 9300:9300 matheusguizolfi/elasticsearch /bin/bash

#INSTALAÇÃO DO LOGSTASH
docker run -ti --name logstash -d --network=elk-connect -p 5000:5000 -p 5044:5044 matheusguizolfi/logstash /bin/bash


#INSTALAÇÃO DO FILEBEAT

docker run -ti --name filebeat -d --network=elk-connect -v /csv/:/var/log/  matheusguizolfi/filebeat /bin/bash

#INSTALAÇÃO DO GRAFANA

docker run -ti --name grafana -d --network=elk-connect -p 3000:3000 matheusguizolfi/grafana /bin/bash

#INSTALAÇÃO DO NGINX

docker run -ti --name nginx -d --network=elk-connect -p 80:80 matheusguizolfi/nginx /bin/bash

#INSTALAÇÃO DO CURATOR

docker run --name curator -d -e INTERVAL_IN_HOURS=24 -e OLDER_THAN_IN_DAYS="2" --network=elk-connect visity/elasticsearch-curator

exit
