#!/bin/bash

set -e

#Inicia o serviço do kibana

/etc/init.d/grafana-server start

# Add kibana como comando se necessário
if [[ "$1" == -* ]]; then
	set -- grafana-server "$@"
fi

exec "$@"
