#!/bin/bash

set -e 

#Inicia o Elasticsearch

/etc/init.d/nginx start


#Add Elasticsearch como um comando se necessário
if [ "${1:0:1}" = '-' ]; then
	set -- nginx "$@"
fi

exec "$@"
