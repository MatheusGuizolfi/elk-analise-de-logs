#!/bin/bash

set -e

#Inicia o serviço do logstash

/etc/init.d/logstash start

# Add logstash como comando se necessário
if [ "${1:0:1}" = '-' ]; then
	set -- logstash "$@"
fi

exec "$@"

