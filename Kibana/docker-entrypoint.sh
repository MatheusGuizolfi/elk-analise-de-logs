#!/bin/bash

set -e

#Inicia o serviço do kibana

/etc/init.d/kibana start

# Add kibana como comando se necessário
if [[ "$1" == -* ]]; then
	set -- kibana "$@"
fi

exec "$@"
