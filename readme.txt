            |-----------------------|    
            |       STACK ELK       |
            |-----------------------|


PRE-REQUISITOS:

1:Você deve ter o Docker instalado e conhecimento básico. Pode encontrar informações nesse link: https://docs.docker.com/engine/installation/
2:Deve ter conhecimento de Docker Network. Pode encontrar informações nesse link: https://docs.docker.com/engine/reference/commandline/network_create/
3:Deve ter o Git instalado e conhecimento básico. Pode encontrar informações nesse link: https://git-scm.com/download/linux
4:Conhecimentos em clone e commit de repositorios. Pode encontrar informações nesse link: https://git-scm.com/book/pt-br/v1/Git-Essencial-Obtendo-um-Reposit%C3%B3rio-Git


MANUAL DE INSTALAÇÃO:

1:Primeiro deve clonar esse repositorio, vai aparecer a pasta "stack-elk".
2:Construa as imagens, acessando cada pasta, e não esqueça, deve estar dentro da pasta da respectiva imagem,depois de verificar isso, execute: docker build -t nomequedesejar .


|--------------------EXECUTE EXATAMENTE NESSA ORDEM----------------------------------|

INSTALAÇÃO DO KIBANA:

comando de instalação do container: docker run -ti --name kibana --network=nomedarede -p 5601:5601 nomedaimagem /bin/bash

INSTALAÇÃO DO ELASTICSEARCH:

Comando de instalação do container: docker run -ti --name elasticsearch --network=nomedarede -p 9200:9200 -p 9300:9300 nomedaimagem /bin/bash

INSTALAÇÃO DO LOGSTASH:

Comando para instalação do container: docker run -ti --name logstash --network=nomedarede -p 5000:5000 -p 5044:5044 nomedaimagem /bin/bash

INSTALAÇÃO DO FILEBEAT:

Comando para a instalação do container: docker run -ti --name filebeat --network=nomedarede -v /pastalogs/:/var/log/  nomedaimagem /bin/bash



|----------------FIM--------------|

OBS: Depois disso já deve estar tudo em funcionamento, qualquer problema revise para verificar se não pulou ou errou nenhum comando.
